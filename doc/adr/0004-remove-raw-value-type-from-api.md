# 4. remove-raw-value-type-from-api

Date: 2020-06-10

## Status

Propsed

[TOC]

## Context

The initial design of the `ReadModel` package was to read aggreagated sensor timeseries in a performant manner. During the project, however, raw sensor values were added to the api. At that moment it was decided to keep raw sensor values separate from aggregated values because of several reasons: 
* number of raw values per sensor were limited; 
* raw values are stored differently by the repository 

Because of this change the overall complexity of this package increased significantly. The `ReadModel` has been in production for several months now. Recently it has been decided to apply an data expiration mechanism to this system. One of the requirements for this system is that it implements _aged based_ expiration rather than _size based_ expiration which applies for both aggregated and raw sensor timeseries. As a result one of the reasons of treating raw values different from aggregated values is no longer valid. 

This document proposes to remove Raw Values from the api. This reduces the complexity significantly and makes it easier to implement the expiration system (which is not the responsibility of the read model library itself).

## Decision 
### `ReadModel` api 

```

type AggregateSpec struct {
	// Function defines how the data is aggregated
	Function AggregateFunc

	// The period unit of the aggregated values
	PeriodUnit PeriodUnit
}

type RangeInput struct {
	ID ID

	// Start of the period
	Start time.Time
	// End of the period
	End time.Time
	// Limit the number of records
	Limit int64
	// Aggregated values are sorted descending by timestamp
	Descending bool

	// Flag to return the last value before Start if no value exactly matches the start time
	IncludeStartValue bool

	// IncludeNextValue specifies whether or not to return the first value beyond the specified time period
	IncludeNextValue bool

	// AggregateFunc defines how the sensor values are aggregated
	AggregateSpec AggregateSpec
}

type RangeOutput struct {
	Values        []SensorValue
	NextValue     *SensorValue
	PreviousValue *SensorValue
}

type ReadModel interface {
	Range(context.Context, RangeInput) (RangeOutput, error)
}
```

This api basically reverts back to the initial behavior of the system which was to query aggregated sensor timeseries. By defining specific input / output types for the Range api it is possible to include more details in the response. 
`RangeInput` is provided with IncludeNextValue / IncludePreviousValue attributes which allows the client to select wheter or not to return these values in the response. 
Instead of returing a slice with aggregated values `Range` operation returns `RangeOutput` to represent the result of the query. This struct includes the slice of values and previous / next values. 

`PeriodUnit` and `AggregateFunc` are specified by `AggregateSpec` of the `RangeInput` struct. Both settings are optional. Raw sensor values are returned if `AggregationSpec` omited. This makes the `Range` operation capable to handle both raw and aggregated values. 

This is a breaking change of course and has impact on the `timeseries-query-api`


### Repository api 
Repository package has been poluted with raw value capabilities which has increased the cyclic complexity of the library significantly. After some discussions in the team it was proposed to remove the raw value entity completely from the api. 

This results in the following modified api definition: 

```
// AggregatedSensorValue represents a stored sensor aggregate
type AggregatedSensorValue struct {
	Period    types.PeriodUnit `json:"period_unit"`
	Timestamp time.Time        `json:"timestamp"`
	Count     int              `json:"count"`
	Average   float64          `json:"average"`
	Min       float64          `json:"min"`
	Max       float64          `json:"max"`
}

type RangeInput struct {
	ID types.ID

	Period types.PeriodUnit

	// Start of the period
	Start time.Time
	// End of the period
	End time.Time
	// Limit the number of records
	Limit int64
	// Aggregated values are sorted descending by timestamp
	Descending bool

	// Flag to return the last value before Start if no value exactly matches the start time
	IncludeStartValue bool

	// IncludeNextValue specifies whether or not to return the first value beyond the specified time period
	IncludeNextValue bool
}

type RangeOutput struct {
	Values        []AggregatedSensorValue
	NextValue     *AggregatedSensorValue
	PreviousValue *AggregatedSensorValue
}

type RepositoryV2 interface {
	// Writes a slice of aggregated values without a transaction.
	Write(types.ID, []AggregatedSensorValue) error

	// Start a new transaction. TransactionalWriter can be used to write records with optimistic locking
	Transactional(types.ID, func(TransactionalWriter) error) error

	// SaveAggregateConfig saves the specified sensor aggregation configuration
	SaveAggregateConfig(config types.SensorAggregateConfig) error

	// GetAggregateConfig reads a SensorAggregationConfig for the specified machine / sensor combination
	GetAggregateConfig(types.ID) (*types.SensorAggregateConfig, error)

	// RangeInput returns all aggregated values which match the specified range
	Range(RangeInput) (*RangeOutput, error)

	// GetAt returns a single AggregatedSensorValue at the specified timestamp
	GetAt(id types.ID, unit types.PeriodUnit, timestamp time.Time) (*AggregatedSensorValue, error)
}
```

Most notable change has been made to the `Range` operation. It now uses its own input struct which is almost identical to types.RangeInput. The major difference is that it does not have the `AggregateFunc` definition. The purpose of this api is to 
> get sensor timeseries for the specified time range / period unit 

By using a specific type this intention can be made clear. The output of the `Range` operation, however, is also changed. `AggregatedSensorValue` struct is now part of the repository package (it used to be part of the `types` package). The name of this struct is still not very clear but it indicates it is a repository specific struct. It basically defines a sensor timeseries value persisted in the repository. We have to work on the naming of this struct a bit more. 

#### Redis Keys
Currently all RawValues are stored under a seperate redis key. Reason behind this is that a RawValue is different from other aggregated data. Allthough this is a valid point it increases complexity significantly.  Instead of just reading and unmarshaling values into a struct, additional logic is required. 

Because `RawValue` is removed from the api the current storage option has to be reconsidered. The following solutions are possible: 

##### Solution 1 : store everything under the same key pattern 

This solution stores both raw values and aggregates under the same key with the same data object. No additional logic is required for all database queries. 

Unfortunately this solution requires a data conversion:
* upgrade timeseries-query-api to latest `ReadModel` package
* deploy `timeseries-query-api` to production 
  * no data returned for raw value queries -> triggers a fallback on redshift 
* deploy new `net-change-lambda` 
  * new raw value aggregates are created for incoming sensor values 
* manually delete the existing `_raw` keys from redis 
* update the `regenerative` package to latest `ReadModel` package

_pros_ 
* simple; no additional logic required while reading data from redis 
* simple; only one struct which represents a persistent value 

_cons_
* data conversion is required -> existing raw data has to be migrated to new key 


##### Solution 2: store raw values / aggregates under different key

This solution is compatible with the current system. Raw values are stored under a different key with it's own specific data object. Compatibility is ensured within the code. Allthough this solution looks tempting it has an important drawback: additional complexity is added to the code. This makes it more difficult to maintain this code in the future. 

_cons_ 
* complex; repository logic has different logic for raw value / aggregate types   

_pros_
* low risk: no data conversion required 


#### OutOfBoundError

The current api might sometimes return an custom error named `OutOfBoundError`. This error indicates that the read model does not have all the data for the requested period. This error is used by the `timeseries-query-api` to perform a _fallback_ query on redshift. 

This error was only returned for `RangeRaw` api which will be removed by this change. The question is now if this error is necesarry or not. 

Some details about the current implementation: this error is returned if there is no previous value while querying raw values. It looks like the improved query can do the same by returning `RangeOutput` without a `PreviousValue` attribute. 

*PROPOSAL* remove this error type from the api since it does not add any value; consumer can achieve exactly the same by checking the `PreviousValue` attribute. 


## Decision
* Remove `RawValue` from the package api
* Remove `OutOfBoundsError`
* Store both raw and aggregated under the same redis-key 

## Consequences

### Consumers

_timeseries-query-api_
* The current version of this service has extensive knowledge of `ReadModel`. Currently the service logic depends on `OutOfBoundError`. This has to be changed; The response type of the `Range` operator (optionally) returns the `PreviousValue` of the specified query. Instead of checking `OutOfBoundError` the client can now check whether or not `PreviousValue` is nil or not. 
* Previous / Next value queries are no longer supported. Client has to specify whether or not to return Previous / Next Value in `RangeInput` 
* `RangeRaw` operation is removed from api. Client has to use the `Range` operation with `PeriodUnit` _PeriodUnitRaw_.

_sensor_read_model_regenerative_batch_
* Not affected 