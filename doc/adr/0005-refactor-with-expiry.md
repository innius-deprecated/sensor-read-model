# 5. refactor-with-expiry

Date: 2020-08-31

## Status

Accepted

[TOC]

## Context

We weren't happy with the complexity and deviations from principles of the original design that have accumulated in the read-model.
We, therefore, decided to try to simplify the read-model and return to its base requirements. 
This started already with ADR-0004 and continues with a revised class diagram (see below).
The read-model library should offer a simple interface that can be implemented by other timeseries database implementations as well.
Currently, the read-model's interface is implemented only by our in-house 'timeseries database' solution.
But in the future we might want to create an adapter for the timeseries database offering of redis-labs as well.
Furthermore, we must include logic for expiry in order to use storage space efficiently and keep response times low.
To do this, we have opted for a scheduled batch process that simply triggers the expiry. All necessary logic will be contained in the read-model library.

## Class Diagram

- **redisTimeSeries**: the timeseries database of redis-labs
- **inniusTimeSeries**: our in-house timeseries database solution, previously known as read-model

![Alt text](2020-09-01-refactor.png)

Note that although a redisTimeseries implementation is depicted in the class diagram above, there is no such implementation to be found in the code.
In the diagram, it serves to exemplify the status and position of the inniusTimeseries object in the read-model scope.

## Changes

#### Internal package
In order to make the read-model library easier to use, all objects related to the innius timeseries database implementation have been moved to an 'internal' directory.
These structs and functions are therefore no longer directly accessible to consumers of this library.

#### Aggregator
Formerly, the aggregator was responsible for aggregating incoming sensor values, interacting with the read-model repository, merging new and existing aggregates, and storing the result.
In the new design, the aggregator stays true to its name and enjoys a reduced scope of responsibility.
It has been demoted to a helper object with two functions, one for aggregating new sensor values, and one for merging new and existing aggregates.
The inniusTimeseries implementation of the external read-model interface now orchestrates the procedure formerly captured by just the aggregator itself.
 
 
#### ExpiryTrigger
The expiryTrigger will serve to implement expiry for the innius timeseries database implementation specifically. 
The read-model library only exports the interface and a function to create the implementation. 
The expiry logic and expiry deadline information is all contained in this library's internals.
