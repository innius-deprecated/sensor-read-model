# 1. Store aggregated values as sorted sets.

Date: 2019-10-01

## Status

Approved

## Context
There are two ways of storing sensor aggregates in redis:
1. create a different key for each aggregate, for example: sensor1~avg~minute~2019-10-32 
2. create a different key for each sensor, aggregate, timeunit compbination and store aggregated values as a stored set

Since the aggregates are always queried by a time range a sorted set has the best fit. 

## Decision

Store aggregates as a stored set 

## Consequences

Data is stored as a stored set;  

 * Increased site of the dataset of a certain redis key. We don't know the exact limits at the moment. 
 * We have to make sure the dataset doesn't get too big.   
 
 
