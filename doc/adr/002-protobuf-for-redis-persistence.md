# 2. Use protobuf for redis performance

Date: 2019-10-07

## Status

Approved

## Context
Given the number of sensors and datapoints it is important to keep the size of redis keys as small as possible. 
Selecting the appropriate serialization method is important. There are multiple possibilities: 
 
* json
This is native golang serialization. Data is stored as key / values; Names of the fields are included in each record.  

* protobuf
Data is stored in a binary format; This is much more efficient than json.  

## Decision
Persisted data is stored in protobuf format.  

## Consequences
* generate golang code for protobuf types
* viewing data with redis-cli is not easy since protobuf data is not a human readable format
* unmarshaling data is a bit more complicated    
 
 
