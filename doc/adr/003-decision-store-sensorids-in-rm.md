# 1. Store and expose sensor ids which are represented in the read model 

Date: 2019-10-07

## Status

Approved

## Context
Multiple clients that need to write to the read model need to know for which sensors to aggregate.
These components are the regenerative and net-change lambdas. 
The regenerative needs a notification when a new sensor id needs to be aggregated.
The net-change needs to be able to know if it needs to process when it receives a metric for a sensor.
In the future we might need to implement an automatic cleanup process that must know which sensors were not read recently.

The clients that read from the read model know for which sensors aggregation should take place.
Sensors might emit few metrics, for example one per hour, at leas we need to take this into account.
Therefore is not possible to deduct the that a sensor is aggregated from the fact that its aggregations exist in the read model. 

So we need some store for the sensor identifiers. 

## Decision

Store sensor ids for which the read model receives a request under a key in the read model store itself. 

## Consequences

The read model package exposes an endpoint which states if a sensor id is known in the read model.
The read model package emits an SQS message when a sensor needs to be aggregated. The message will have a *5 minute delay* 
 
 
