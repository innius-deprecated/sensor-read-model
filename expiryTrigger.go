package sensor_read_model

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository"
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository/adapters/redisadapter"
	"bitbucket.org/innius/sensor-read-model/v2/types"
)

type expiryTrigger struct {
	repo repository.RepositoryV2
}

func NewExpiryTrigger(redisAddr string) (types.ExpiryTrigger, error) {
	repo, err := redisadapter.NewDefaultRepository(redisAddr)
	if err != nil {
		return nil, err
	}

	return &expiryTrigger{
		repo: repo,
	}, nil
}

func (e *expiryTrigger) Expire() {
	e.repo.Expire()
}
