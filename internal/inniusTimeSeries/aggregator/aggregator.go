package aggregator

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository"
	"bitbucket.org/innius/sensor-read-model/v2/types"
	"math"
	"sort"
	"time"
)

func New() Aggregator {
	return &aggregator{}
}

type aggregator struct {
	expiryScheme interface{} // TODO
}

func (a *aggregator) Aggregate(v []types.SensorValue) ([]repository.AggregatedSensorValue, error) {
	vv := toRawValueAggregates(v)

	result := []repository.AggregatedSensorValue{}
	for _, unit := range types.PeriodUnitsAggregated {
		result = append(result, vv.aggregate(unit)...)
	}
	return result, nil
}

func (a *aggregator) Merge(new repository.AggregatedSensorValue, existing repository.AggregatedSensorValue) repository.AggregatedSensorValue {
	return merge(new, existing)
}

func merge(new repository.AggregatedSensorValue, existing repository.AggregatedSensorValue) repository.AggregatedSensorValue {
	avg := func(a float64, c int) float64 {
		return a * float64(c)
	}

	existing.Average = (avg(new.Average, new.Count) + avg(existing.Average, existing.Count)) / float64(new.Count+existing.Count)
	existing.Count += new.Count
	existing.Max = math.Max(new.Max, existing.Max)
	existing.Min = math.Min(new.Min, existing.Min)

	return existing
}

type aggregatedValues []repository.AggregatedSensorValue

func (a aggregatedValues) Period(p types.PeriodUnit) (time.Time, time.Time) {
	if len(a) == 0 {
		return time.Time{}, time.Time{}
	}
	sort.Slice(a, func(i, j int) bool {
		return a[i].Timestamp.After(a[j].Timestamp)
	})

	start, end := a[0].Timestamp, a[len(a)-1].Timestamp

	return p.Round(start), p.Last(end)
}

func toRawValueAggregates(v []types.SensorValue) aggregatedValues {
	result := make(aggregatedValues, len(v))
	for i, value := range v {
		result[i] = repository.AggregatedSensorValue{
			Average:   value.Value,
			Max:       value.Value,
			Min:       value.Value,
			Timestamp: value.Timestamp,
			Period:    types.PeriodUnitSecond,
			Count:     1,
		}
	}
	return result
}

func (v aggregatedValues) aggregate(unit types.PeriodUnit) aggregatedValues {
	result := map[time.Time]repository.AggregatedSensorValue{}

	for _, value := range v {
		value.Period = unit
		value.Timestamp = unit.Round(value.Timestamp)
		var current repository.AggregatedSensorValue
		if cv, found := result[value.Timestamp]; found {
			current = cv
			result[value.Timestamp] = merge(value, current)
		} else {
			result[value.Timestamp] = value
		}
	}

	aggr := aggregatedValues{}
	for _, value := range result {
		aggr = append(aggr, value)
	}
	return aggr
}
