package aggregator

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository"
	"bitbucket.org/innius/sensor-read-model/v2/types"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"
)

type expectedValue struct {
	timestamp string
	unit      types.PeriodUnit
	Count     int
	Average   float64
	Min       float64
	Max       float64
}

func (exp expectedValue) AggregatedSensorValue() repository.AggregatedSensorValue {
	timestamp, _ := time.Parse(time.RFC3339, exp.timestamp)
	return repository.AggregatedSensorValue{
		Period:    exp.unit,
		Timestamp: timestamp,
		Count:     exp.Count,
		Average:   exp.Average,
		Min:       exp.Min,
		Max:       exp.Max,
	}

}
func TestAggregator(t *testing.T) {
	Convey("add new values to an empty repo", t, func() {
		date, _ := time.Parse(time.RFC3339, "2019-10-02T18:43:02Z")

		values := []types.SensorValue{}
		for i := 0; i < 100; i++ {
			values = append(values, types.SensorValue{
				Timestamp: date.Add(time.Duration(i) * time.Second),
				Value:     float64(i),
			})
		}

		aggr := New()

		res, err := aggr.Aggregate(values)
		So(err, ShouldBeNil)

		expected := []expectedValue{
			{timestamp: "2019-10-02T18:43:00Z", unit: types.PeriodUnitMinute, Count: 58, Average: 28.5, Min: 0, Max: 57},
			{timestamp: "2019-10-02T18:44:00Z", unit: types.PeriodUnitMinute, Count: 42, Average: 78.5, Min: 58, Max: 99},
			{timestamp: "2019-10-02T18:00:00Z", unit: types.PeriodUnitHour, Count: 100, Average: 49.5, Min: 0, Max: 99},
			{timestamp: "2019-10-02T16:00:00Z", unit: types.PeriodUnit8Hours, Count: 100, Average: 49.5, Min: 0, Max: 99},
			{timestamp: "2019-10-02T00:00:00Z", unit: types.PeriodUnitDay, Count: 100, Average: 49.5, Min: 0, Max: 99},
			{timestamp: "2019-10-01T00:00:00Z", unit: types.PeriodUnitMonth, Count: 100, Average: 49.5, Min: 0, Max: 99},
		}

		So(res, ShouldHaveLength, len(values)+6)
		for i := range expected {
			So(res, ShouldContain, expected[i].AggregatedSensorValue())
		}
	})
}

//func newExpectedValue(ts string, unit types.PeriodUnit, count int, average, min, max float64) expectedValue {
//	return expectedValue{timestamp: ts, unit: unit, Count: count, Average: average, Min: min, Max: max}
//}
//
//func newTestValue(ts string, unit types.PeriodUnit, count int, average, min, max float64) repository.AggregatedSensorValue {
//	return newExpectedValue(ts, unit, count, average, min, max).AggregatedSensorValue()
//}

// TODO evaluate test case
// TODO convert to system/integration test
//func TestAggregateValuesSpanTwoMonths(t *testing.T) {
//	Convey("add values at the end of the month", t, func() {
//		id := types.ID{MachineID: "", SensorID: ""}
//		date, _ := time.Parse(time.RFC3339, "2019-10-31T23:58:13Z")
//		r := &mockRepo{
//			data: []repository.AggregatedSensorValue{
//				newTestValue("2019-10-31T23:58:00Z", types.PeriodUnitMinute, 2, 4, 0, 4),
//				newTestValue("2019-10-31T23:59:00Z", types.PeriodUnitMinute, 2, 4, 0, 4),
//				newTestValue("2019-11-01T00:01:00Z", types.PeriodUnitMinute, 2, 5, 0, 5),
//			},
//		}
//		aggr := New(r)
//
//		values := []types.SensorValue{
//			{Timestamp: date, Value: 1},
//			{Timestamp: date.Add(time.Minute), Value: 2},
//			{Timestamp: date.Add(2 * time.Minute), Value: 3},
//			{Timestamp: date.Add(3 * time.Minute), Value: 4},
//		}
//
//		So(aggr.Merge(id, values), ShouldBeNil)
//
//		So(r.data, ShouldHaveLength, len(values) + 4+2+2+2) // raw values, four minutes, 2 hours, 2 days and two months
//	})
//}

func TestAggregateMerge(t *testing.T) {
	Convey("Merge aggregated values", t, func() {
		timestamp, _ := time.Parse(time.RFC3339, "2019-10-18T10:09:00Z")
		existing := repository.AggregatedSensorValue{
			Period:    types.PeriodUnitMinute,
			Timestamp: timestamp,
			Count:     4,
			Average:   178.61504324999999,
			Min:       160.282121,
			Max:       201.138348,
		}
		new := repository.AggregatedSensorValue{
			Period:    types.PeriodUnitMinute,
			Timestamp: timestamp,
			Count:     82,
			Average:   197.66971046341467,
			Min:       160.282121,
			Max:       201.138348,
		}
		aggr := New()
		merged := aggr.Merge(existing, new)

		So(merged.Count, ShouldEqual, existing.Count+new.Count)
		So(merged.Average, ShouldEqual, 196.78344687209307)
		So(merged.Period, ShouldEqual, types.PeriodUnitMinute)
		So(merged.Timestamp, ShouldEqual, timestamp)
		So(merged.Max, ShouldEqual, existing.Max)
		So(merged.Min, ShouldEqual, existing.Min)
	})
}
