package aggregator

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository"
	"bitbucket.org/innius/sensor-read-model/v2/types"
)

// Aggregator is the api which changes the state of the read model. The name of this interface is all but clear.
// We have to find a better name for this interface. Perhaps something like `SensorTimeseriesStore` or something like that.
// At the end, that's what it is. It is a store for sensor timeseries.
type Aggregator interface {
	// Aggregate takes a slice of sensor values and aggregates them
	Aggregate([]types.SensorValue) ([]repository.AggregatedSensorValue, error)

	// Merge takes a slice of new and existing sensor aggregates and merges them into each other.
	Merge(new repository.AggregatedSensorValue, existing repository.AggregatedSensorValue) repository.AggregatedSensorValue
}
