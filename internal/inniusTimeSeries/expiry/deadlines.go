package expiry

import (
	"bitbucket.org/innius/sensor-read-model/v2/types"
"time"
)

type ExpiryDeadlineFunc func(p types.PeriodUnit) time.Time

// retention scheme
// Unit	Expiration Period
// raw	    24h
// minute	14days
// hour		6M
// 8 hour	1y
// day	    2Y
// month	?? (for now 4y)
func GetExpiryDeadlineFunc(from time.Time) ExpiryDeadlineFunc {
	return func(p types.PeriodUnit) time.Time {
		switch p {
		case types.PeriodUnitSecond:
			return from.AddDate(0, 0, -1)
		case types.PeriodUnitMinute:
			return from.AddDate(0, 0, -14)
		case types.PeriodUnitHour:
			return from.AddDate(0, -6, 0)
		case types.PeriodUnit8Hours:
			return from.AddDate(-1, 0, 0)
		case types.PeriodUnitDay:
			return from.AddDate(-2, 0, 0)
		case types.PeriodUnitMonth:
			return from.AddDate(-4, 0, 0)
		default:
			return from
		}
	}
}
