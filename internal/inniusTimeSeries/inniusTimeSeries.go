package inniusTimeSeries

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/aggregator"
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/expiry"
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository"
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository/adapters/redisadapter"
	"bitbucket.org/innius/sensor-read-model/v2/types"
	"context"
	"github.com/avast/retry-go"
	"github.com/jonboulle/clockwork"
	"github.com/sirupsen/logrus"
)

type InniusTimeSeriesDatabase interface {
	Store(context.Context, types.ID, []types.SensorValue) error
	Range(context.Context, types.RangeInput) (*types.RangeOutput, error)
	Expire()
}

type inniusTimeSeriesDatabase struct {
	clock        clockwork.Clock
	ExpiryScheme interface{} // TODO
	Aggregator   aggregator.Aggregator
	repo         repository.RepositoryV2
}

func NewInniusTimeSeriesDatabase(redisAddr string) (InniusTimeSeriesDatabase, error) {
	repo, err := redisadapter.NewDefaultRepository(redisAddr)
	if err != nil {
		return nil, err
	}
	return &inniusTimeSeriesDatabase{
		clock:      clockwork.NewRealClock(),
		repo:       repo,
		Aggregator: aggregator.New(),
	}, nil
}

func (db *inniusTimeSeriesDatabase) Store(c context.Context, sensorID types.ID, values []types.SensorValue) error {
	// aggregate
	newAggregates, err := db.Aggregator.Aggregate(values)
	if err != nil {
		logrus.Errorf("could not aggregate sensorValues; Error: %+v", err)
		return err
	}
	newAggregates = db.filter(newAggregates)
	return retry.Do(func() error {
		return db.transactionalWrite(sensorID, newAggregates)
	}, retry.OnRetry(func(n uint, err error) {
		logrus.Warnf("retry writing sensor values for sensor %s [%d]; %+v", sensorID.SensorID, n, err)
	}))
}

func (db *inniusTimeSeriesDatabase) transactionalWrite(sensorID types.ID, newAggregates []repository.AggregatedSensorValue) error {
	return db.repo.Transactional(sensorID, func(tx repository.TransactionalWriter) error {
		for i, aggr := range newAggregates {
			// get existing
			var existing *repository.AggregatedSensorValue
			var err error
			if aggr.Period != types.PeriodUnitSecond {
				existing, err = db.repo.GetAt(sensorID, aggr.Period, aggr.Timestamp)
				if err != nil {
					logrus.Errorf("could not read existing aggregates for sensorID %+v; Error: %+v", sensorID, err)
					return err
				}
			}
			// TODO test this nil pointer check
			if existing != nil {
				// merge
				existing := repository.AggregatedSensorValue{
					Period:    existing.Period,
					Timestamp: existing.Timestamp,
					Count:     existing.Count,
					Average:   existing.Average,
					Min:       existing.Min,
					Max:       existing.Max,
				}
				newAggregates[i] = db.Aggregator.Merge(aggr, existing)
			}
		}

		// write all new values to the database
		return tx.Write(newAggregates)
	})
}
func (db *inniusTimeSeriesDatabase) filter(list []repository.AggregatedSensorValue) []repository.AggregatedSensorValue {
	var result []repository.AggregatedSensorValue
	for i := range list {
		if !db.expired(list[i]) {
			result = append(result, list[i])
		}
	}
	return result
}

// expired checks if an AggregatedSensorValue is expired
func (db inniusTimeSeriesDatabase) expired(v repository.AggregatedSensorValue) bool {
	deadline := expiry.GetExpiryDeadlineFunc(db.clock.Now())
	return v.Timestamp.Before(deadline(v.Period))
}

func (db *inniusTimeSeriesDatabase) Range(c context.Context, input types.RangeInput) (*types.RangeOutput, error) {
	res, err := db.repo.Range(repository.RangeInput{
		ID:                input.ID,
		Period:            input.PeriodUnit,
		Start:             input.Start,
		End:               input.End,
		Limit:             input.Limit,
		Descending:        input.Descending,
		IncludeStartValue: input.IncludeStartValue,
		IncludeNextValue:  input.IncludeNextValue,
	})
	if err != nil {
		return nil, err
	}

	var nextValue *types.SensorValue
	if res.NextValue != nil {
		nextValue = &types.SensorValue{
			Timestamp: res.NextValue.Timestamp,
			Value:     getValue(*res.NextValue),
		}
	}

	var previousValue *types.SensorValue
	if res.PreviousValue != nil {
		previousValue = &types.SensorValue{
			Timestamp: res.PreviousValue.Timestamp,
			Value:     getValue(*res.PreviousValue),
		}
	}

	values := make([]types.SensorValue, len(res.Values))
	for i, v := range res.Values {
		values[i] = types.SensorValue{
			Timestamp: v.Timestamp,
			Value:     getValue(v),
		}
	}

	return &types.RangeOutput{
		Values:        values,
		NextValue:     nextValue,
		PreviousValue: previousValue,
	}, nil
}

func getValue(value repository.AggregatedSensorValue) float64 {
	return value.Average
}

func (db *inniusTimeSeriesDatabase) Expire() {
	return
}
