package inniusTimeSeries

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/aggregator"
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository"
	"bitbucket.org/innius/sensor-read-model/v2/types"
	"context"
	"github.com/jonboulle/clockwork"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

type mockStore struct {
	repository.RepositoryV2
	rangeResult                   *repository.RangeOutput
	getAtResult                   *repository.AggregatedSensorValue
	transactionalWriteInvocations int
	data                          []repository.AggregatedSensorValue
}

func (m *mockStore) Range(opts repository.RangeInput) (*repository.RangeOutput, error) {
	return m.rangeResult, nil
}

func (m *mockStore) Write(types.ID, []repository.AggregatedSensorValue) error {
	return nil
}

func (m *mockStore) GetAt(id types.ID, unit types.PeriodUnit, timestamp time.Time) (*repository.AggregatedSensorValue, error) {
	return m.getAtResult, nil
}

// Start a new transaction. TransactionalWriter can be used to write records with optimistic locking
func (m *mockStore) Transactional(id types.ID, f func(writer repository.TransactionalWriter) error) error {
	return f(&mockTransaction{store: m})
}

type mockTransaction struct {
	store *mockStore
	repository.TransactionalWriter
}

// transactional write
func (m *mockTransaction) Write(in []repository.AggregatedSensorValue) error {
	m.store.data = in
	m.store.transactionalWriteInvocations++
	return nil
}

type mockAggregator struct {
	aggregator.Aggregator
	aggregateResponse    []repository.AggregatedSensorValue
	mergeResponse        repository.AggregatedSensorValue
	aggregateInvocations int
	mergeInvocations     int
}

func (m *mockAggregator) Aggregate([]types.SensorValue) ([]repository.AggregatedSensorValue, error) {
	m.aggregateInvocations++
	return m.aggregateResponse, nil
}

func (m *mockAggregator) Merge(new repository.AggregatedSensorValue, existing repository.AggregatedSensorValue) repository.AggregatedSensorValue {
	m.mergeInvocations++
	return m.mergeResponse
}

type inniusTimeSeriesDatabaseTestWrapper struct {
	inniusTimeSeriesDatabase
}

func (i *inniusTimeSeriesDatabaseTestWrapper) mockAggregator() *mockAggregator {
	return i.Aggregator.(*mockAggregator)
}

func (i *inniusTimeSeriesDatabaseTestWrapper) mockStore() *mockStore {
	return i.repo.(*mockStore)
}

func newTestInniusTimeseries(store repository.RepositoryV2, clock clockwork.Clock) *inniusTimeSeriesDatabaseTestWrapper {
	return &inniusTimeSeriesDatabaseTestWrapper{
		inniusTimeSeriesDatabase{
			clock:        clock,
			ExpiryScheme: nil,
			Aggregator:   &mockAggregator{},
			repo:         store,
		},
	}
}

func TestNextValue(t *testing.T) {
	store := &mockStore{}
	store.rangeResult = &repository.RangeOutput{
		Values: nil,
		NextValue: &repository.AggregatedSensorValue{
			Timestamp: time.Now(),
			Average:   10.00,
		},
		PreviousValue: nil,
	}
	m := newTestInniusTimeseries(store, clockwork.NewFakeClock())
	opts := types.RangeInput{}
	result, err := m.Range(context.Background(), opts)
	assert.NoError(t, err)
	assert.Equal(t, store.rangeResult.NextValue.Timestamp, result.NextValue.Timestamp)
	assert.Equal(t, store.rangeResult.NextValue.Average, result.NextValue.Value)
}

func TestPreviousValue(t *testing.T) {
	store := &mockStore{}
	store.rangeResult = &repository.RangeOutput{
		PreviousValue: &repository.AggregatedSensorValue{
			Timestamp: time.Now(),
			Average:   10.00,
		},
	}
	m := newTestInniusTimeseries(store, clockwork.NewFakeClock())
	opts := types.RangeInput{}
	result, err := m.Range(context.Background(), opts)
	assert.NoError(t, err)
	assert.Equal(t, store.rangeResult.PreviousValue.Timestamp, result.PreviousValue.Timestamp)
	assert.Equal(t, store.rangeResult.PreviousValue.Average, result.PreviousValue.Value)
}

func TestRangeRawHappy(t *testing.T) {
	// prepare
	id := types.ID{"mid", "sid"}
	store := &mockStore{}
	vals := []repository.AggregatedSensorValue{{Timestamp: parseTime("2019-12-04T16:54:00Z")}}
	store.rangeResult = &repository.RangeOutput{
		Values:        vals,
		NextValue:     nil,
		PreviousValue: nil,
	}
	sut := newTestInniusTimeseries(store, clockwork.NewFakeClock())
	rng := types.RangeInput{
		ID: id,
	}

	// exercise
	actual, _ := sut.Range(context.Background(), rng)

	// assert
	assert.Len(t, actual.Values, len(vals))
	//assert.Equal(t, vals, actual.Values)
}

func TestStore(t *testing.T) {
	c := context.Background()
	id := types.ID{
		MachineID: "",
		SensorID:  "",
	}
	date, _ := time.Parse(time.RFC3339, "2019-10-02T18:43:02Z")

	values := make([]repository.AggregatedSensorValue, 5)
	for i := range values {
		values[i] = repository.AggregatedSensorValue{
			Period:    types.PeriodUnitDay,
			Timestamp: date.Add(time.Duration(i) * time.Second),
			Count:     1,
			Average:   0,
			Min:       0,
			Max:       0,
		}
	}

	r := &mockStore{}
	ts := newTestInniusTimeseries(r, clockwork.NewFakeClock())

	a := ts.mockAggregator()
	s := ts.mockStore()

	a.aggregateResponse = values
	a.mergeResponse = repository.AggregatedSensorValue{
		Period:    types.PeriodUnitDay,
		Timestamp: date,
		Count:     1,
		Average:   0,
		Min:       0,
		Max:       0,
	}
	s.getAtResult = &repository.AggregatedSensorValue{
		Period:    types.PeriodUnitDay,
		Timestamp: date,
		Count:     1,
		Average:   0,
		Min:       0,
		Max:       0,
	}

	aggrInvocInit := a.aggregateInvocations
	mergeInvocInit := a.mergeInvocations

	storeInvocInit := s.transactionalWriteInvocations

	err := ts.Store(c, id, []types.SensorValue{{Timestamp: date, Value: 1}})
	assert.Nil(t, err)

	assert.EqualValues(t, aggrInvocInit+1, a.aggregateInvocations)
	assert.EqualValues(t, mergeInvocInit+len(values), a.mergeInvocations)
	assert.EqualValues(t, storeInvocInit+1, s.transactionalWriteInvocations)
}

func Test_ExpirationSchema(t *testing.T) {
	tests := []struct {
		unit      types.PeriodUnit
		timestamp string
		expired   bool
	}{
		{types.PeriodUnitSecond, "2020-02-22T22:00:00Z", false},
		{types.PeriodUnitSecond, "2020-02-21T22:00:00Z", true},
		{types.PeriodUnitMinute, "2020-02-22T22:00:00Z", false},
		{types.PeriodUnitMinute, "2020-02-07T22:00:00Z", true},
		{types.PeriodUnitHour, "2020-02-22T22:00:00Z", false},
		{types.PeriodUnitHour, "2019-08-07T22:00:00Z", true},
		{types.PeriodUnitDay, "2020-02-22T22:00:00Z", false},
		{types.PeriodUnitDay, "2018-01-07T22:00:00Z", true},
		{types.PeriodUnitMonth, "2020-02-22T22:00:00Z", false},
		{types.PeriodUnitMonth, "2015-01-07T22:00:00Z", true},
	}

	sut := newTestInniusTimeseries(nil, clockwork.NewFakeClockAt(parseTime("2020-02-22T23:00:00Z")))

	for _, test := range tests {
		v := repository.AggregatedSensorValue{
			Period:    test.unit,
			Timestamp: parseTime(test.timestamp),
		}
		assert.Equal(t, test.expired, sut.expired(v))
	}

}

func TestFilterExpiredItems(t *testing.T) {
	in := []repository.AggregatedSensorValue{
		{types.PeriodUnitSecond, parseTime("2020-02-22T22:00:00Z"), 4, 3.00, 1.00, 6.00},
		{types.PeriodUnitSecond, parseTime("2020-02-22T23:00:00Z"), 4, 5.00, 2.00, 8.00},
		{types.PeriodUnitSecond, parseTime("2020-02-23T00:00:00Z"), 8, 4.00, 1.00, 8.00},
		{types.PeriodUnitHour, parseTime("2020-02-23T01:00:00Z"), 8, 4.00, 1.00, 8.00},
	}

	sut := newTestInniusTimeseries(nil, clockwork.NewFakeClockAt(parseTime("2020-03-22T22:00:00Z")))

	result := sut.filter(in)

	assert.Len(t, result, 1)
}

func parseTime(rfc3339 string) time.Time {
	r, _ := time.Parse(time.RFC3339, rfc3339)
	return r
}
