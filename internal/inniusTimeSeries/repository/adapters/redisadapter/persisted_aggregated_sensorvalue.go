package redisadapter

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository"
	"bitbucket.org/innius/sensor-read-model/v2/types"
	"github.com/golang/protobuf/proto"
	"time"
)

func fromAggregatedSensorValue(v repository.AggregatedSensorValue) PersistedAggregatedSensorValue {
	return PersistedAggregatedSensorValue{
		PeriodUnit: int32(v.Period),
		Average:    v.Average,
		Min:        v.Min,
		Max:        v.Max,
		Count:      int32(v.Count),
		Timestamp:  v.Timestamp.Unix(),
	}
}

func (m PersistedAggregatedSensorValue) MarshalBinary() ([]byte, error) {
	return proto.Marshal(&m)
}

func (m *PersistedAggregatedSensorValue) UnmarshalBinary(data []byte) error {
	return proto.Unmarshal(data, m)
}

func (m PersistedAggregatedSensorValue) AggregatedSensorValue() repository.AggregatedSensorValue {
	// Convert time to utc is important but only relevant if redis runs in a non-utc timezone
	// In production the redis will run in utc because aws services are always in utc
	return repository.AggregatedSensorValue{
		Period:    types.PeriodUnit(m.PeriodUnit),
		Timestamp: time.Unix(m.Timestamp, 0).UTC(),
		Count:     int(m.Count),
		Average:   m.Average,
		Min:       m.Min,
		Max:       m.Max,
	}
}
