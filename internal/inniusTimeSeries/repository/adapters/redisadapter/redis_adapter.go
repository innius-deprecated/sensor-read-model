package redisadapter

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository"
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/expiry"
	"bitbucket.org/innius/sensor-read-model/v2/types"
	"fmt"
	"github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
	"time"
)

func NewDefaultRepository(redisAddr string) (repository.RepositoryV2, error) {
	return NewFromCreator(redisAddr, createRealClient)
}

func createRealClient(addr string) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
}

// decouples the redis implementation
func NewFromCreator(addr string, createClient func(addr string) *redis.Client) (repository.RepositoryV2, error) {
	rs, err := newRedisClientAndPing(addr, createClient)
	if err != nil {
		return nil, err
	}
	return &redisAdapterV2{Client: rs}, nil
}

func newRedisClientAndPing(addr string, createClient func(addr string) *redis.Client) (*redis.Client, error) {
	client := createClient(addr)
	_, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}
	return client, nil
}

type transaction struct {
	sensorID types.ID
	*redis.Tx
}

func (t *transaction) Write(values []repository.AggregatedSensorValue) error {
	for key, updatedValues := range groupByPeriodUnit(t.sensorID, values) {
		_, err := t.Pipelined(func(pipe redis.Pipeliner) error {
			members := toRedisMembers(updatedValues)
			// remove existing keys from redis; this does not apply for sensor value aggregates because they will
			// never be updated since second is the highest possible frequency
			if key.PeriodUnit != types.PeriodUnitSecond {
				for _, z := range members {
					score := fmt.Sprintf("%.f", z.Score)
					pipe.ZRemRangeByScore(key.String(), score, score) // intentionally ignore any error
				}
			}
			return pipe.ZAdd(key.String(), members...).Err()
		})
		if err != nil {
			return err
		}
	}
	return nil
}

type redisAdapterV2 struct {
	*redis.Client
}

func (adapter *redisAdapterV2) GetAt(id types.ID, unit types.PeriodUnit, timestamp time.Time) (*repository.AggregatedSensorValue, error) {
	at := fmt.Sprintf("%d", timestamp.Unix())
	opt := &redis.ZRangeBy{Min: at, Max: at, Count: 1}
	v := []PersistedAggregatedSensorValue{}
	err := adapter.ZRevRangeByScore(sensorKey(id, unit), opt).ScanSlice(&v)
	if err != nil {
		return nil, fmt.Errorf("redis ZRevRangeByScore error; %w", err)
	}

	if len(v) == 0 {
		return nil, nil
	}
	res := v[0].AggregatedSensorValue()
	return &res, nil
}

// Returns the last value before range Start, the range Start value itself if it is an exact match or an OutOfBoundsError if not found
func (adapter redisAdapterV2) previousValue(rng repository.RangeInput) (*repository.AggregatedSensorValue, error) {
	min := "0"
	max := fmt.Sprintf("(%d", rng.Start.Unix())
	opt := &redis.ZRangeBy{Min: min, Max: max, Count: 1}
	v := []PersistedAggregatedSensorValue{}
	err := adapter.ZRevRangeByScore(sensorKey(rng.ID, rng.Period), opt).ScanSlice(&v)
	if err != nil {
		return nil, fmt.Errorf("redis ZRevRangeByScore error; %w", err)
	}

	if len(v) == 0 {
		return nil, nil
	}
	res := v[0].AggregatedSensorValue()
	return &res, nil
}

func (adapter redisAdapterV2) nextValue(input repository.RangeInput) (*repository.AggregatedSensorValue, error) {
	min := fmt.Sprintf("(%d", input.End.Unix())
	max := fmt.Sprintf("%d", input.End.AddDate(0, 1, 0).Unix()) // we expect a next value within a month time

	opt := &redis.ZRangeBy{Min: min, Max: max, Count: 1}
	v := []PersistedAggregatedSensorValue{}
	err := adapter.ZRangeByScore(sensorKey(input.ID, input.Period), opt).ScanSlice(&v)
	if err != nil {
		return nil, fmt.Errorf("redisAdapterV2.nextValue() - redis ZRangeByScore error; %w", err)
	}

	if len(v) == 0 {
		return nil, nil
	}

	res := v[0].AggregatedSensorValue()
	return &res, nil
}

func (adapter redisAdapterV2) Transactional(lockOnID types.ID, f func(repository.TransactionalWriter) error) error {
	return adapter.Watch(func(tx *redis.Tx) error {
		return f(&transaction{Tx: tx, sensorID: lockOnID})
	}, sensorKeys(lockOnID)...)
}

func (adapter redisAdapterV2) Write(id types.ID, values []repository.AggregatedSensorValue) error {
	for key, updatedValues := range groupByPeriodUnit(id, values) {
		members := toRedisMembers(updatedValues)
		for _, z := range members {
			score := fmt.Sprintf("%.f", z.Score)
			adapter.ZRemRangeByScore(key.String(), score, score)
		}
		err := adapter.ZAdd(key.String(), members...).Err()
		if err != nil {
			return errors.Wrapf(err, "could not write aggregated values for sensor %+v", id)
		}
	}
	return nil
}

func (adapter redisAdapterV2) Range(rng repository.RangeInput) (*repository.RangeOutput, error) {
	opt := &redis.ZRangeBy{Min: fmt.Sprintf("%d", rng.Start.Unix()), Max: fmt.Sprintf("%d", rng.End.Unix()), Count: rng.Limit}
	var v []PersistedAggregatedSensorValue
	if err := adapter.zrangeByScore(sensorKey(rng.ID, rng.Period), opt, rng.Descending).ScanSlice(&v); err != nil {
		return nil, fmt.Errorf("redis zrange error; %w", err)
	}

	values := make([]repository.AggregatedSensorValue, len(v))
	for i := range v {
		values[i] = v[i].AggregatedSensorValue()
	}

	res := &repository.RangeOutput{
		Values: values,
	}
	if rng.IncludeNextValue {
		nextValue, err := adapter.nextValue(rng)
		if err != nil {
			return nil, err
		}
		res.NextValue = nextValue
	}

	if rng.IncludeStartValue {
		previousValue, err := adapter.previousValue(rng)
		if err != nil {
			return nil, err
		}
		res.PreviousValue = previousValue
	}

	return res, nil
}

func (adapter *redisAdapterV2) Expire() {
	deadlinesByUnit := getExpiryDeadlinesByUnit()

	for _, u := range types.PeriodUnitsAggregated {
		keys := adapter.getKeysForUnit(u)
		for _, key := range keys {
			adapter.Client.ZRemRangeByScore(key, "-inf", fmt.Sprintf("%v", deadlinesByUnit[u].Unix()))
		}
	}
}

func getExpiryDeadlinesByUnit() map[types.PeriodUnit]time.Time {
	deadline := expiry.GetExpiryDeadlineFunc(time.Now())
	deadlineByUnit := make(map[types.PeriodUnit]time.Time, len(types.PeriodUnitsAggregated))
	for _, u := range types.PeriodUnitsAggregated {
		deadlineByUnit[u] = deadline(u)
	}
	return deadlineByUnit
}

func (adapter *redisAdapterV2) getKeysForUnit(unit types.PeriodUnit) []string {
	return adapter.Client.Keys(fmt.Sprintf("*~%d", unit)).Val()
}

func reverse(aggrs []repository.AggregatedSensorValue) {
	for i := len(aggrs)/2 - 1; i >= 0; i-- {
		opp := len(aggrs) - 1 - i
		aggrs[i], aggrs[opp] = aggrs[opp], aggrs[i]
	}
}

func (adapter *redisAdapterV2) zrangeByScore(key string, opt *redis.ZRangeBy, descending bool) *redis.StringSliceCmd {
	if descending {
		return adapter.ZRevRangeByScore(key, opt)
	}
	return adapter.ZRangeByScore(key, opt)
}

type redisKey struct {
	SensorID   types.ID
	PeriodUnit types.PeriodUnit
}

func (k redisKey) String() string {
	return sensorKey(k.SensorID, k.PeriodUnit)
}

func groupByPeriodUnit(sensorID types.ID, a []repository.AggregatedSensorValue) map[redisKey][]repository.AggregatedSensorValue {
	values := map[redisKey][]repository.AggregatedSensorValue{}
	for _, v := range a {

		key := redisKey{SensorID: sensorID, PeriodUnit: v.Period}
		ms, found := values[key]
		if !found {
			values[key] = []repository.AggregatedSensorValue{v}
		} else {
			values[key] = append(ms, v)
		}
	}
	return values
}

func toRedisMembers(values []repository.AggregatedSensorValue) []*redis.Z {
	members := make([]*redis.Z, len(values))
	for i, v := range values {
		members[i] = &redis.Z{Member: fromAggregatedSensorValue(v), Score: float64(v.Timestamp.Unix())}
	}
	return members
}

func sensorKeys(sensorID types.ID) []string {
	res := make([]string, len(types.PeriodUnitsAggregated))
	for i, p := range types.PeriodUnitsAggregated {
		res[i] = sensorKey(sensorID, p)
	}
	return res
}

func sensorKey(sensorID types.ID, period types.PeriodUnit) string {
	return fmt.Sprintf("%s~%s~%d", sensorID.MachineID, sensorID.SensorID, period)
}
