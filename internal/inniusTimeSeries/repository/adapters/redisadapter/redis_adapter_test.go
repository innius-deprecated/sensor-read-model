package redisadapter

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries/repository"
	"bitbucket.org/innius/sensor-read-model/v2/types"
	"errors"
	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis/v7"
	. "github.com/smartystreets/goconvey/convey" //TODO: remove this package since two test frameworks in one file is a bit too much
	"github.com/stretchr/testify/assert"
	"github.com/taskcluster/slugid-go/slugid"
	"sync"
	"testing"
	"time"
)

// Tests the redis adapter against a running redis service.
// There's *two versions* of each test. One is executing the system against an in process redis implementation.
// The other runs against an external redis service on port 6379. You need to spin up a docker container.
// If the test cannot connect to redis, it will ignore the test.

func createInProcessRedisClient(dummy string) *redis.Client {
	mr, _ := miniredis.Run()
	return redis.NewClient(&redis.Options{
		Addr: mr.Addr(),
	})
}

//
//func TestRedisAdapterSensorWriteRangeRawInProcessRedis(t *testing.T) {
//	testRedisAdapterSensorWriteRangeRaw(t, createInProcessRedisClient)
//}
//
//func TestRedisAdapterSensorWriteRangeRawExternalRedis(t *testing.T) {
//	testRedisAdapterSensorWriteRangeRaw(t, createRealClient)
//}
//
//func testRedisAdapterSensorWriteRangeRaw(t *testing.T, createClient func(addr string) *redis.Client) {
//	sutItf, err := NewFromCreator("localhost:6379", createClient)
//	if err != nil {
//		t.Skip("Error connecting to redis, aborting test")
//	}
//	sut := sutItf.(*redisAdapterV2)
//
//	sensorID := types.ID{MachineID: "machineId", SensorID: "sensorId"}
//
//	values := []types.SensorValue{
//		types.SensorValue{parseTime("2019-11-28T13:00:00Z"), 3},
//		types.SensorValue{parseTime("2019-11-28T13:01:00Z"), 1},
//		types.SensorValue{parseTime("2019-11-28T13:02:00Z"), 2},
//		types.SensorValue{parseTime("2019-11-28T13:03:00Z"), 4},
//	}
//
//	writeRawLimited(sut, sensorID, values, 3)
//
//	start := parseTime("2019-11-28T13:01:00Z")
//	rng := types.RangeRaw{
//		ID:    sensorID,
//		Start: start,
//		End:   start.Add(1 * time.Minute),
//	}
//	actual, _ := sut.RangeRaw(rng)
//
//	// the oldest was removed
//	expected := []types.SensorValue{
//		types.SensorValue{parseTime("2019-11-28T13:01:00Z"), 1},
//		types.SensorValue{parseTime("2019-11-28T13:02:00Z"), 2},
//	}
//
//	// compare in json because it rounds the float values, we can't compare floats because go does not support explicit struct equality
//	aS0Bytes, _ := json.Marshal(actual)
//	eS0Bytes, _ := json.Marshal(expected)
//	assert.JSONEq(t, string(eS0Bytes), string(aS0Bytes))
//}

func TestRedisAdapterSensorRangePrevValueRawInProcessRedis(t *testing.T) {
	testRedisAdapterSensorRangePrevValueRaw(t, createInProcessRedisClient)
}

func TestRedisAdapterSensorRangePrevValueRawExternalRedis(t *testing.T) {
	testRedisAdapterSensorRangePrevValueRaw(t, createRealClient)
}

func testRedisAdapterSensorRangePrevValueRaw(t *testing.T, createClient func(addr string) *redis.Client) {
	sutItf, err := NewFromCreator("localhost:6379", createClient)
	if err != nil {
		t.Skip("Error connecting to redis, aborting test")
	}
	sut := sutItf.(*redisAdapterV2)

	sensorID := types.ID{MachineID: "machineId", SensorID: "sensorId"}

	values := []repository.AggregatedSensorValue{
		{Timestamp: parseTime("2019-11-28T13:00:00Z")},
		{Timestamp: parseTime("2019-11-28T13:01:00Z")},
		{Timestamp: parseTime("2019-11-28T13:02:00Z")},
		{Timestamp: parseTime("2019-11-28T13:03:00Z")},
	}

	sut.Write(sensorID, values)

	rng := repository.RangeInput{
		ID:                sensorID,
		Start:             parseTime("2019-11-28T13:01:30Z"), // between second and third actual values timestamps
		End:               parseTime("2019-11-28T13:03:00Z"),
		IncludeStartValue: true, // include start value
	}
	actual, _ := sut.Range(rng)

	expected := &repository.AggregatedSensorValue{
		Timestamp: parseTime("2019-11-28T13:01:00Z"),
	}

	assert.Len(t, actual.Values, 2)
	assert.EqualValues(t, expected, actual.PreviousValue)
}

func TestRedisAdapter_NextValue(t *testing.T) {
	testRedisAdapter_NextValue(t, createRealClient)
}

func testRedisAdapter_NextValue(t *testing.T, createClient func(addr string) *redis.Client) {
	sutItf, err := NewFromCreator("localhost:6379", createClient)
	if err != nil {
		t.Skip("Error connecting to redis, aborting test")
	}
	sut := sutItf.(*redisAdapterV2)

	sensorID := types.ID{MachineID: "machineId", SensorID: "sensorId"}

	values := []repository.AggregatedSensorValue{
		{Timestamp: parseTime("2019-11-28T13:00:00Z")},
		{Timestamp: parseTime("2019-11-28T13:01:00Z")},
		{Timestamp: parseTime("2019-11-28T13:02:00Z")},
	}

	sut.Write(sensorID, values)

	rng := repository.RangeInput{
		ID:                sensorID,
		Start:             values[1].Timestamp,
		End:               parseTime("2019-11-28T13:01:00Z"),
		IncludeStartValue: true, // include start value
	}
	actual, _ := sut.nextValue(rng)

	assert.Equal(t, "2019-11-28T13:02:00Z", actual.Timestamp.Format(time.RFC3339))
}

func TestRedisAdapter_PreviousValue(t *testing.T) {
	testRedisAdapter_PreviousValue(t, createRealClient)
}

func testRedisAdapter_PreviousValue(t *testing.T, createClient func(addr string) *redis.Client) {
	sutItf, err := NewFromCreator("localhost:6379", createClient)
	if err != nil {
		t.Skip("Error connecting to redis, aborting test")
	}
	sut := sutItf.(*redisAdapterV2)

	sensorID := types.ID{MachineID: "machineId", SensorID: "sensorId"}

	values := []repository.AggregatedSensorValue{
		{Timestamp: parseTime("2019-11-28T13:00:00Z")},
		{Timestamp: parseTime("2019-11-28T13:01:00Z")},
		{Timestamp: parseTime("2019-11-28T13:02:00Z")},
	}

	sut.Write(sensorID, values)

	rng := repository.RangeInput{
		ID:                sensorID,
		Start:             values[1].Timestamp,
		End:               parseTime("2019-11-28T13:01:00Z"),
		IncludeStartValue: true, // include start value
	}
	actual, _ := sut.previousValue(rng)

	assert.Equal(t, "2019-11-28T13:00:00Z", actual.Timestamp.Format(time.RFC3339))
}
//
//func TestRedisAdapterSensorRangeAggregatesRawInProcessRedis(t *testing.T) {
//	testRedisAdapterSensorRangeAggregatesRaw(t, createInProcessRedisClient)
//}
//
//func TestRedisAdapterSensorRangeAggregatesRawExternalRedis(t *testing.T) {
//	testRedisAdapterSensorRangeAggregatesRaw(t, createRealClient)
//}
//
//func testRedisAdapterSensorRangeAggregatesRaw(t *testing.T, createClient func(addr string) *redis.Client) {
//	sutItf, err := NewFromCreator("localhost:6379", createClient)
//	if err != nil {
//		t.Skip("Error connecting to redis, aborting test")
//	}
//	sut := sutItf.(*redisAdapterV2)
//
//	sensorID := types.ID{MachineID: "machineId", SensorID: "sensorId"}
//
//	values := []types.SensorValue{
//		{parseTime("2019-11-28T13:00:01Z"), 3},
//		{parseTime("2019-11-28T13:00:02Z"), 1},
//		{parseTime("2019-11-28T13:00:03Z"), 2},
//	}
//
//	writeRawLimited(sut, sensorID, values, 1000) // retain em all
//
//	rng := types.RangeInput{
//		ID:         sensorID,
//		Period:     types.PeriodUnitSecond,
//		Start:      parseTime("2019-11-28T13:00:01Z"),
//		End:        parseTime("2019-11-28T13:00:03Z"),
//		Limit:      0,
//		Descending: false,
//	}
//	actual, _ := sut.Range(rng)
//
//	expected := []repository.AggregatedSensorValue{
//		{types.PeriodUnitSecond, parseTime("2019-11-28T13:00:01Z"), 1, 3, 3, 3},
//		{types.PeriodUnitSecond, parseTime("2019-11-28T13:00:02Z"), 1, 1, 1, 1},
//		{types.PeriodUnitSecond, parseTime("2019-11-28T13:00:03Z"), 1, 2, 2, 2},
//	}
//
//	// compare in json because it rounds the float values, we can't compare floats because go does not support explicit struct equality
//	aS0Bytes, _ := json.Marshal(actual)
//	eS0Bytes, _ := json.Marshal(expected)
//	assert.JSONEq(t, string(eS0Bytes), string(aS0Bytes))
//}
//
func TestRedisAdapterSensorRangeDescAggregatesRawInProcessRedis(t *testing.T) {
	testRedisAdapterSensorRangeDescAggregatesRaw(t, createInProcessRedisClient)
}

func TestRedisAdapterSensorRangeDescAggregatesRawExternalRedis(t *testing.T) {
	testRedisAdapterSensorRangeDescAggregatesRaw(t, createRealClient)
}

func testValues(unit types.PeriodUnit, ts ...string) []repository.AggregatedSensorValue {
	var res []repository.AggregatedSensorValue
	for _, t := range ts {
		res = append(res, newAggrSensorValue(unit, t))
	}
	return res
}

func newAggrSensorValue(unit types.PeriodUnit, timestamp string) repository.AggregatedSensorValue {
	return repository.AggregatedSensorValue{
		Period:    unit,
		Timestamp: parseTime(timestamp),
		Count:     0,
		Average:   0,
		Min:       0,
		Max:       0,
	}
}

func testRedisAdapterSensorRangeDescAggregatesRaw(t *testing.T, createClient func(addr string) *redis.Client) {
	sutItf, err := NewFromCreator("localhost:6379", createClient)
	if err != nil {
		t.Skip("Error connecting to redis, aborting test")
	}
	sut := sutItf.(*redisAdapterV2)

	sensorID := types.ID{MachineID: "machineId", SensorID: "sensorId"}

	values := testValues(types.PeriodUnitSecond, "2019-11-28T13:00:01Z", "2019-11-28T13:00:02Z", "2019-11-28T13:00:03Z")

	sut.Write(sensorID, values)

	rng := repository.RangeInput{
		ID:         sensorID,
		Period:     types.PeriodUnitSecond,
		Start:      parseTime("2019-11-28T13:00:01Z"),
		End:        parseTime("2019-11-28T13:00:03Z"),
		Limit:      0,
		Descending: true,
	}
	actual, _ := sut.Range(rng)

	// reversed order
	expected := testValues(types.PeriodUnitSecond, "2019-11-28T13:00:03Z", "2019-11-28T13:00:02Z", "2019-11-28T13:00:01Z")

	assert.EqualValues(t, expected, actual.Values)
}

func TestRedisAdapterSensorRawAfterInProcessRedis(t *testing.T) {
	testRedisAdapterSensorRawAfter(t, createInProcessRedisClient)
}

func TestRedisAdapterSensorRawAfterExternalRedis(t *testing.T) {
	testRedisAdapterSensorRawAfter(t, createRealClient)
}

func testRedisAdapterSensorRawAfter(t *testing.T, createClient func(addr string) *redis.Client) {
	sutItf, err := NewFromCreator("localhost:6379", createClient)
	if err != nil {
		t.Skip("Error connecting to redis, aborting test")
	}
	sut := sutItf.(*redisAdapterV2)

	sensorID := types.ID{MachineID: "machineId", SensorID: "sensorId"}

	values := testValues(types.PeriodUnitSecond, "2019-11-28T13:00:00Z", "2019-11-28T13:01:00Z", "2019-11-28T13:02:00Z", "2019-11-28T13:03:00Z")

	sut.Write(sensorID, values)

	ra := repository.RangeInput{
		ID:                sensorID,
		Period:            types.PeriodUnitSecond,
		Start:             parseTime("2019-11-28T13:00:30Z"),
		End:               parseTime("2019-11-28T13:01:30Z"),
		Limit:             0,
		Descending:        false,
		IncludeStartValue: false,
		IncludeNextValue:  true,
	}

	actual, _ := sut.Range(ra)

	expected := newAggrSensorValue(types.PeriodUnitSecond, "2019-11-28T13:02:00Z")

	assert.EqualValues(t, &expected, actual.NextValue)
}

//func testRedisAdapterSensorWriteDeletesAndRangeRawOob(t *testing.T, createClient func(addr string) *redis.Client) {
//	sutItf, err := NewFromCreator("localhost:6379", createClient)
//	if err != nil {
//		t.Skip("Error connecting to redis, aborting test")
//	}
//	sut := sutItf.(*redisAdapterV2)
//
//	sensorID := types.ID{MachineID: "machineId", SensorID: "sensorId"}
//
//	values := []types.SensorValue{
//		types.SensorValue{parseTime("2019-11-28T13:00:00Z"), 3},
//		types.SensorValue{parseTime("2019-11-28T13:01:00Z"), 1},
//		types.SensorValue{parseTime("2019-11-28T13:02:00Z"), 2},
//		types.SensorValue{parseTime("2019-11-28T13:03:00Z"), 4},
//	}
//
//	writeRawLimited(sut, sensorID, values, 3)
//
//	start := parseTime("2019-11-28T13:00:30Z")
//	rng := types.RangeRaw{
//		ID:    sensorID,
//		Start: start,
//		End:   start.Add(2 * time.Minute),
//	}
//	_, actualErr := sut.RangeRaw(rng)
//
//	// the oldest was removed, oldest value present is 13:01:00, so 13:00:30 is expected out of bounds
//	assert.NotNil(t, actualErr)
//	actualErrType := reflect.TypeOf(actualErr).String()
//	assert.EqualValues(t, "*repository.OutOfBoundsError", actualErrType)
//}
//
func parseTime(rfc3339 string) time.Time {
	r, _ := time.Parse(time.RFC3339, rfc3339)
	return r
}

func TestRedisAdapterV2_GetAt(t *testing.T) {
	testRedisAdapterV2_GetAt(t, createInProcessRedisClient)
}

type clientFactory func(string) *redis.Client

func testRedisAdapterV2_GetAt(t *testing.T, factory clientFactory) {
	store, err := NewFromCreator("localhost:6379", factory)
	if err != nil {
		t.Skip("Error connecting to redis, aborting test")
	}
	id := types.ID{
		MachineID: slugid.V4(),
		SensorID:  slugid.V4(),
	}
	start := time.Date(2019, 9, 27, 7, 21, 31, 00, time.UTC)
	tests := []repository.AggregatedSensorValue{
		{Period: types.PeriodUnitMinute, Timestamp: start, Average: 1},
		{Period: types.PeriodUnitMinute, Timestamp: start.Add(time.Minute), Average: 2},
		{Period: types.PeriodUnitMinute, Timestamp: start.Add(5 * time.Minute), Average: 3},
		{Period: types.PeriodUnitHour, Timestamp: start.Truncate(time.Hour), Average: 55},
	}

	if err := store.Write(id, tests); err != nil {
		t.Fatal(err)
	}

	res, err := store.GetAt(id, types.PeriodUnitHour, tests[3].Timestamp)
	assert.NoError(t, err)
	if assert.NotNil(t, res) {
		assert.EqualValues(t, &tests[3], res)
	}
	t.Run("get a non existent value", func(t *testing.T) {
		res, err := store.GetAt(id, types.PeriodUnitMonth, start)
		assert.NoError(t, err)
		assert.Nil(t, res)
	})
}

func TestRedisAdapter_TransactionalWriteInProcessRedis(t *testing.T) {
	testRedisAdapter_TransactionalWrite(t, createInProcessRedisClient)
}
func TestRedisAdapter_TransactionalWriteExternalRedis(t *testing.T) {
	testRedisAdapter_TransactionalWrite(t, createRealClient)
}
func testRedisAdapter_TransactionalWrite(t *testing.T, factory clientFactory) {
	Convey("RedisValueStoreTest", t, func() {
		store, err := NewFromCreator("localhost:6379", factory)
		if err != nil {
			t.Skip("Error connecting to redis, aborting test")
		}

		So(err, ShouldBeNil)
		So(store, ShouldNotBeNil)

		sensorID := types.ID{MachineID: slugid.Nice(), SensorID: slugid.Nice()}
		start := time.Date(2019, 9, 27, 7, 21, 31, 00, time.UTC)
		tests := []repository.AggregatedSensorValue{
			{Period: types.PeriodUnitMinute, Timestamp: start, Average: 1},
			{Period: types.PeriodUnitMinute, Timestamp: start.Add(time.Minute), Average: 2},
			{Period: types.PeriodUnitMinute, Timestamp: start.Add(5 * time.Minute), Average: 3},
			{Period: types.PeriodUnitHour, Timestamp: start.Truncate(time.Hour), Average: 55},
		}
		So(store.Transactional(sensorID, func(tx repository.TransactionalWriter) error {
			return tx.Write(tests)
		}), ShouldBeNil)

		Convey("Query the repository", func() {
			Convey("aggregates per minute", func() {
				unit := types.PeriodUnitMinute
				res, err := store.Range(repository.RangeInput{
					ID:     sensorID,
					Period: unit,
					Start:  start,
					End:    start.Add(2 * time.Minute),
				})
				So(err, ShouldBeNil)
				values := res.Values
				So(values, ShouldNotBeNil)
				So(values, ShouldHaveLength, 2)
				So(values[0].Timestamp, ShouldEqual, start)

				Convey("query hourly aggregate", func() {
					res, err := store.Range(repository.RangeInput{
						ID:     sensorID,
						Period: types.PeriodUnitHour,
						Start:  start.Round(time.Hour),
						End:    start.Add(2 * time.Hour),
					})
					So(err, ShouldBeNil)
					values = res.Values
					So(values, ShouldNotBeNil)
					So(values, ShouldHaveLength, 1)
				})
				Convey("get only one value", func() {
					unit := types.PeriodUnitMinute
					res, err := store.Range(repository.RangeInput{
						ID:     sensorID,
						Period: unit,
						Start:  start,
						End:    start.Add(2 * time.Minute),
						Limit:  1,
					})
					So(err, ShouldBeNil)
					values := res.Values
					So(values, ShouldNotBeNil)
					So(values, ShouldHaveLength, 1)
					So(values[0].Timestamp, ShouldEqual, start)
				})

				Convey("in descending order", func() {
					unit := types.PeriodUnitMinute
					res, err := store.Range(repository.RangeInput{
						ID:         sensorID,
						Period:     unit,
						Start:      start,
						End:        start.Add(2 * time.Minute),
						Descending: true,
					})
					So(err, ShouldBeNil)
					values := res.Values
					So(values, ShouldNotBeNil)
					So(values, ShouldHaveLength, 2)
					So(values[1].Timestamp, ShouldEqual, start)
				})
			})
			Convey("aggregates per hour", func() {
				start := start.Truncate(time.Hour)
				unit := types.PeriodUnitHour
				res, err := store.Range(repository.RangeInput{
					ID:     sensorID,
					Period: unit,
					Start:  start,
					End:    start.Add(2 * time.Hour),
				})
				So(err, ShouldBeNil)
				values := res.Values
				So(values, ShouldNotBeNil)
				So(values, ShouldHaveLength, 1)
				So(values[0].Timestamp, ShouldEqual, start)
			})
			Convey("update an existing set", func() {
				newValue := []repository.AggregatedSensorValue{{Period: types.PeriodUnitMinute, Timestamp: start, Average: 10}}

				So(store.Write(sensorID, newValue), ShouldBeNil)

				res, err := store.Range(repository.RangeInput{
					ID:     sensorID,
					Period: types.PeriodUnitMinute,
					Start:  start,
					End:    start.Add(1 * time.Second),
				})
				So(err, ShouldBeNil)
				values := res.Values
				So(values, ShouldNotBeNil)
				So(values, ShouldHaveLength, 1)
				So(values[0].Average, ShouldEqual, newValue[0].Average)
			})
		})

		Convey("Store identical values at different timestamps", func() {
			values := []repository.AggregatedSensorValue{
				{Period: types.PeriodUnitMinute, Timestamp: start, Average: 1},
				{Period: types.PeriodUnitMinute, Timestamp: start.Add(time.Minute), Average: 1},
			}
			sensorID := types.ID{
				MachineID: slugid.Nice(),
				SensorID:  slugid.Nice(),
			}
			So(store.Write(sensorID, values), ShouldBeNil)

			res, err := store.Range(repository.RangeInput{
				ID:     sensorID,
				Period: types.PeriodUnitMinute,
				Start:  start,
				End:    start.Add(time.Hour),
			})

			So(err, ShouldBeNil)
			rng := res.Values
			So(rng, ShouldHaveLength, len(values))
		})
	})
}

//func TestRedisAdapter_AggregateAfterInProcessRedis(t *testing.T) {
//	testRedisAdapter_AggregateAfter(t, createInProcessRedisClient)
//}
//func TestRedisAdapter_AggregateAfterExternalRedis(t *testing.T) {
//	testRedisAdapter_AggregateAfter(t, createRealClient)
//}
//func testRedisAdapter_AggregateAfter(t *testing.T, createClient func(addr string) *redis.Client) {
//	store, err := NewFromCreator("localhost:6379", createClient)
//	if err != nil {
//		t.Skip("Error connecting to redis, aborting test")
//	}
//
//	sensorID := types.ID{MachineID: slugid.Nice(), SensorID: slugid.Nice()}
//	start := parseTime("2019-09-27T07:21:31Z")
//	tests := []repository.AggregatedSensorValue{
//		{Period: types.PeriodUnitMinute, Timestamp: start, Average: 1},
//		{Period: types.PeriodUnitMinute, Timestamp: start.Add(time.Minute), Average: 2},
//		{Period: types.PeriodUnitMinute, Timestamp: start.Add(5 * time.Minute), Average: 3},
//	}
//	store.Write(sensorID, tests)
//
//	unit := types.PeriodUnitMinute
//	actual, _ := store.AggregateAfter(types.AggregateAfterSpec{
//		ID:            sensorID,
//		Period:        unit,
//		AggregateTime: start.Add(2 * time.Minute),
//	})
//
//	expected := repository.AggregatedSensorValue{
//		Period:    types.PeriodUnitMinute,
//		Timestamp: start.Add(5 * time.Minute),
//		Count:     0,
//		Average:   3,
//		Min:       0,
//		Max:       0,
//	}
//
//	assert.Equal(t, expected, *actual)
//}
//
func TestRedisAdapterV2_Transactional(t *testing.T) {
	Convey("RedisAdapterV2Test", t, func() {
		store, err := NewFromCreator("localhost:6379", createRealClient)
		if err != nil {
			t.Skip("Error connecting to redis, aborting test")
		}
		So(err, ShouldBeNil)
		id := types.ID{MachineID: slugid.Nice(), SensorID: slugid.Nice()}
		start := time.Date(2019, 9, 27, 7, 21, 31, 00, time.UTC)
		values := []repository.AggregatedSensorValue{
			{Period: types.PeriodUnitMinute, Timestamp: start, Average: 1},
			{Period: types.PeriodUnitMinute, Timestamp: start.Add(time.Minute), Average: 2},
			{Period: types.PeriodUnitMinute, Timestamp: start.Add(5 * time.Minute), Average: 3},
			{Period: types.PeriodUnitHour, Timestamp: start.Truncate(time.Hour), Average: 55},
		}

		err = store.Transactional(id, func(tx repository.TransactionalWriter) error {
			current, _ := store.Range(repository.RangeInput{})
			So(current, ShouldNotBeNil)
			return tx.Write(values)
		})
		So(err, ShouldBeNil)

		res, err := store.Range(repository.RangeInput{
			ID:     id,
			Period: types.PeriodUnitMinute,
			Start:  start,
			End:    start.Add(time.Hour),
		})
		So(err, ShouldBeNil)
		retrieved := res.Values
		So(retrieved, ShouldHaveLength, 3)

		Convey("Concurrency", func(c C) {
			semaphore := make(chan struct{})
			newValue := []repository.AggregatedSensorValue{{Period: types.PeriodUnitMinute, Timestamp: start, Average: 102}}
			wg := sync.WaitGroup{}
			wg.Add(1)
			go func(c C) {
				err := store.Transactional(id, func(tx repository.TransactionalWriter) error {
					<-semaphore
					err := tx.Write(newValue)
					return err
				})

				c.So(errors.Is(err, redis.TxFailedErr), ShouldBeTrue)
				wg.Done()
			}(c)

			// add another value for the same sensor with another time unit; lock is placed on sensorid, this should cause an error
			So(store.Write(id, []repository.AggregatedSensorValue{{Period: types.PeriodUnitHour, Timestamp: start, Average: 102}}), ShouldBeNil)
			semaphore <- struct{}{}
			wg.Wait()
		})
	})
}

func TestRedisAdapterV2_Expire(t *testing.T) {
	testRedisAdapterV2_Expire(t, createRealClient)
}

func testRedisAdapterV2_Expire(t *testing.T, factory clientFactory) {
	store, err := NewFromCreator("localhost:6379", factory)
	if err != nil {
		t.Skip("Error connecting to redis, aborting test")
	}
	id := types.ID{
		MachineID: slugid.V4(),
		SensorID:  slugid.V4(),
	}
	start := time.Now()
	year := 365 * 24 * time.Hour

	expired := make([]repository.AggregatedSensorValue, len(types.PeriodUnitsAggregated))

	for i, u := range types.PeriodUnitsAggregated {
		expired[i] = repository.AggregatedSensorValue{Period: u, Timestamp: start.Add(-10 * year), Average: float64(i)}
	}

	if err := store.Write(id, expired); err != nil {
		t.Fatal(err)
	}

	notExpired := make([]repository.AggregatedSensorValue, len(types.PeriodUnitsAggregated))

	for i, u := range types.PeriodUnitsAggregated {
		notExpired[i] = repository.AggregatedSensorValue{Period: u, Timestamp: start.Add(-1 * time.Minute), Average: float64(10 + i)}
	}

	if err := store.Write(id, notExpired); err != nil {
		t.Fatal(err)
	}

	rangefunc := func(u types.PeriodUnit) repository.RangeOutput {
		res, err := store.Range(repository.RangeInput{
			ID:                id,
			Period:            u,
			Start:             time.Now().Add(-11 * year),
			End:               time.Now(),
		})
		assert.Nil(t, err)
		assert.NotNil(t, res)

		return *res
	}

	for _, u := range types.PeriodUnitsAggregated {
		res := rangefunc(u)
		assert.Len(t, res.Values, 2)
	}

	t.Run("Expire", func(t *testing.T) {
		store.Expire()

		for _, u := range types.PeriodUnitsAggregated {
			res := rangefunc(u)
			assert.Len(t, res.Values, 1)
		}
	})
}
