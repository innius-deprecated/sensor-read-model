package repository

import (
	"bitbucket.org/innius/sensor-read-model/v2/types"
	"time"
)

// AggregatedSensorValue represents a stored sensor aggregate
type AggregatedSensorValue struct {
	Period    types.PeriodUnit `json:"period_unit"`
	Timestamp time.Time        `json:"timestamp"`
	Count     int              `json:"count"`
	Average   float64          `json:"average"`
	Min       float64          `json:"min"`
	Max       float64          `json:"max"`
}

type RangeInput struct {
	ID types.ID

	Period types.PeriodUnit

	// Start of the period
	Start time.Time
	// End of the period
	End time.Time
	// Limit the number of records
	Limit int64
	// Aggregated values are sorted descending by timestamp
	Descending bool

	// Flag to return the last value before Start if no value exactly matches the start time
	IncludeStartValue bool

	// IncludeNextValue specifies whether or not to return the first value beyond the specified time period
	IncludeNextValue bool
}

type RangeOutput struct {
	Values        []AggregatedSensorValue
	NextValue     *AggregatedSensorValue
	PreviousValue *AggregatedSensorValue
}

// TODO evaluate options for using context in redis calls
type RepositoryV2 interface {
	// Writes a slice of aggregated values without a transaction.
	Write(types.ID, []AggregatedSensorValue) error

	// Start a new transaction. TransactionalWriter can be used to write records with optimistic locking
	Transactional(types.ID, func(TransactionalWriter) error) error

	// RangeInput returns all aggregated values which match the specified range
	Range(RangeInput) (*RangeOutput, error)

	// GetAt returns a single AggregatedSensorValue at the specified timestamp
	GetAt(id types.ID, unit types.PeriodUnit, timestamp time.Time) (*AggregatedSensorValue, error)

	Expire()
}

type TransactionalWriter interface {
	// Writes the raw values and aggregated values in one transaction
	Write([]AggregatedSensorValue) error
}
