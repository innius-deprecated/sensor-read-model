package sensor_read_model

import (
	"bitbucket.org/innius/sensor-read-model/v2/internal/inniusTimeSeries"
	"bitbucket.org/innius/sensor-read-model/v2/types"
	"context"
)

// New creates a new instance of types.TimeSeriesDatabase
func NewDefault(redisAddr string) (types.TimeSeriesDatabase, error) {
	ts, err := inniusTimeSeries.NewInniusTimeSeriesDatabase(redisAddr)
	if err != nil {
		return nil, err
	}

	return &inniusTimeSeriesAdapter{ts: ts}, nil
}

type inniusTimeSeriesAdapter struct {
	ts inniusTimeSeries.InniusTimeSeriesDatabase
}

func (rm *inniusTimeSeriesAdapter) Store(c context.Context, sensorID types.ID, v []types.SensorValue) error {
	return rm.ts.Store(c, sensorID, v)
}

// RangeInput performs the specified query
func (rm *inniusTimeSeriesAdapter) Range(c context.Context, opts types.RangeInput) (*types.RangeOutput, error) {
	return rm.ts.Range(c, opts)
}
