# sensor-read-model 

Golang library for innius sensor read model 

## usage 

```
bitbucket.org/innius/sensor-read-model
```

```golang
store := readmodel.NewRedisStore("localhost:6379")
```

### Decisions 
* use optimistic locking while updating redis; see https://redis.io/topics/transactions#cas
* performance tests for different redis storage options (keys vs sorted sets) are skipped for now

## dependencies

This module is refered to from at least the projects listed below.
It is wise to update these components on each realease of this sensor-read-model

* timeseries-query-api
* continuous-sensor-aggregate-net-change
* aggregation-trigger

## Protobuf

The read model applies Google's Protocol Buffers to shrink the data amount stored in redis.
Data payload definitions are specified in `.proto` files, these are the protocol buffer definitions.
Compling the buffer definitions to go code is a *manual* step, it's not included in the build process. 
When the `.proto` files are extended, the programmer needs to install the protoc and protoc-gen-go and run the generator against the definitions.
The resulting generated `.pb.go` need to be added to the version control system. See <https://github.com/golang/protobuf>
