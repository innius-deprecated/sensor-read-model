package types

import "context"

// TimeSeriesDatabase is the api which is used by all reading consumers
// This name is not very clear. Perhaps we should rename this to `SensorTimeSeries` or
// `SensorTimeseriesQuery`
type TimeSeriesDatabase interface {
	Range(context.Context, RangeInput) (*RangeOutput, error)
	Store(context.Context, ID, []SensorValue) error
}

type ExpiryTrigger interface {
	Expire()
}
