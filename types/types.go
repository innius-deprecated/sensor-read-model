package types

import (
	"github.com/jinzhu/now"
	"time"
)

// AggregationType defines how data is aggregated
type AggregationType uint8

const (
	None AggregationType = iota
	Average
)

type PeriodUnit uint8

const (
	PeriodUnitMinute PeriodUnit = iota
	PeriodUnitHour
	PeriodUnitDay
	PeriodUnitMonth
	PeriodUnitSecond
	PeriodUnit8Hours
)

// convenience vars which contains all supported unit; used for looping
// used for aggregation so it does not include raw
var PeriodUnitsAggregated = []PeriodUnit{
	PeriodUnitMinute, PeriodUnitHour, PeriodUnit8Hours, PeriodUnitDay, PeriodUnitMonth, PeriodUnitSecond,
}

func (p PeriodUnit) Round(t time.Time) time.Time {
	switch p {
	case PeriodUnitHour:
		return now.New(t).BeginningOfHour()
	case PeriodUnit8Hours:
		return round8Hours(t)
	case PeriodUnitMonth:
		return now.New(t).BeginningOfMonth()
	case PeriodUnitDay:
		return now.New(t).BeginningOfDay()
	case PeriodUnitSecond:
		return now.New(t).Truncate(time.Second)
	}
	return now.New(t).BeginningOfMinute()
}
func round8Hours(t time.Time) time.Time {
	roundedHour := time.Duration((t.Hour() / 8) * 8)
	return now.New(t).BeginningOfDay().Add(roundedHour * time.Hour)
}

func (p PeriodUnit) Last(t time.Time) time.Time {
	switch p {
	case PeriodUnitHour:
		return now.New(t).EndOfHour()
	case PeriodUnit8Hours:
		return endOf8HoursFrom(t)
	case PeriodUnitMonth:
		return now.New(t).EndOfMonth()
	case PeriodUnitDay:
		return now.New(t).EndOfDay()
	case PeriodUnitSecond:
		return t.Truncate(time.Second)
	}
	return now.New(t).EndOfMinute()
}

func endOf8HoursFrom(t time.Time) time.Time {
	return now.New(round8Hours(t).Add(7 * time.Hour)).EndOfHour()
}

type ID struct {
	MachineID string `json:"machine_id"`
	SensorID  string `json:"sensor_id"`
}

// RangeInput defines a query for sensor timeseries
type RangeInput struct {
	ID ID

	// The period unit of the aggregated values
	PeriodUnit PeriodUnit

	// Start of the period
	Start time.Time
	// End of the period
	End time.Time
	// Limit the number of records
	Limit int64
	// Aggregated values are sorted descending by timestamp
	Descending bool

	// IncludeStartValue specifies if the last datapoint before the start of the period should be returned
	IncludeStartValue bool

	// IncludeNextValue specifies whether or not to return the first value beyond the specified time period
	IncludeNextValue bool

	// AggregationType defines how the sensor values are aggregated
	Aggregation AggregationType
}

// RangeOutput defines the result of an sensor timeseries query
type RangeOutput struct {
	// Values are the actual values of the specified query
	Values []SensorValue

	// NextValue is the first value after the specified period
	NextValue *SensorValue

	// PreviousValue is the first value before the specified period
	PreviousValue *SensorValue
}

// SensorValue is a sensor value with a timestamp
type SensorValue struct {
	Timestamp time.Time
	Value     float64
}
