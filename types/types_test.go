package types

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

// Asserts that the ordinals (positions) of the enum values remains unchanged because outside world depends on it
// Adding values at the end is typically OK. Change values only with care.
func TestPeriodUnit_Ordinal(t *testing.T) {
	pus := []PeriodUnit{
		PeriodUnitMinute,
		PeriodUnitHour,
		PeriodUnitDay,
		PeriodUnitMonth,
		PeriodUnitSecond,
		PeriodUnit8Hours}
	expected := []PeriodUnit{PeriodUnit(0), PeriodUnit(1), PeriodUnit(2), PeriodUnit(3), PeriodUnit(4), PeriodUnit(5)}
	assert.Equal(t, expected, pus)
}

func TestPeriodUnit_Round_8Hours(t *testing.T) {
	in := parseTime("2020-01-22T21:43:31Z")
	actual := PeriodUnit8Hours.Round(in)
	expected := parseTime("2020-01-22T16:00:00Z")
	assert.Equal(t, expected, actual)
}

func TestPeriodUnit_Last_8Hours(t *testing.T) {
	in := parseTime("2020-01-22T21:43:31Z")
	actual := PeriodUnit8Hours.Last(in)
	expected := parseTime("2020-01-22T23:59:59.999999999Z")
	assert.Equal(t, expected, actual)
}

func parseTime(rfc3339 string) time.Time {
	t, _ := time.Parse(time.RFC3339, rfc3339)
	return t
}
